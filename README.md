# README #


## Spring boot实现WebUploader 文件上传后台 ##

WebUploader是一个简单的以HTML5为主，FLASH为辅的现代文件上传组件。在现代的浏览器里面能充分发挥HTML5的优势，同时又不摒弃主流IE浏览器，延用原来的FLASH运行时，兼容IE6+，Andorid 4+，IOS 6+。两套运行时，同样的调用方式，可供用户任意选用。

支持大文件分片并发上传，极大的提高了文件上传效率。

- 官网： http://fex.baidu.com/webuploader/